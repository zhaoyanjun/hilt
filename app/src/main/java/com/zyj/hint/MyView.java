package com.zyj.hint;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.zyj.hint.util.Util;
import com.zyj.hint.util.ViewUtil;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

/**
 * @Description:
 * @author: zhaoyj
 * @date: 2022/12/22
 */
@AndroidEntryPoint
public class MyView extends AppCompatTextView {

    @Inject
    Util appToken;

    @Inject
    ViewUtil viewUtil;

    public MyView(Context context) {
        super(context);
        init();
    }

    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        String token = appToken.token;
        Log.d("yu--", "view1 ViewScoped " + viewUtil);
    }
}
