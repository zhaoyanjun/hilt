package com.zyj.hint

import android.util.Log
import androidx.lifecycle.ViewModel
import com.zyj.hint.util.ViewModelUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var viewModelUtil: ViewModelUtil

    fun run() {
        Log.d("yu--", "MainViewModel $viewModelUtil")
    }
}