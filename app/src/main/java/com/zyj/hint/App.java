package com.zyj.hint;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

/**
 * @Description:
 * @author: zhaoyj
 * @date: 2022/12/22
 */
@HiltAndroidApp
public class App extends Application {
}
