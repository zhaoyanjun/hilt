package com.zyj.hint

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.zyj.hint.util.ActivityUtil
import com.zyj.hint.util.SingletonUtil
import com.zyj.hint.util.Util
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var appToken: Util

    @Inject
    lateinit var singletonUtil: SingletonUtil

    @Inject
    lateinit var activityUtil: ActivityUtil

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tv = findViewById<TextView>(R.id.tv)
        tv.text = appToken.token
        tv.setOnClickListener {
            startActivity(Intent(this, MainActivity2::class.java))
        }

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.run()
    }
}