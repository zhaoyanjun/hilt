
package com.zyj.hint;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.zyj.hint.util.ViewUtil;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

/**
 * @Description:
 * @author: zhaoyj
 * @date: 2022/12/22
 */
@AndroidEntryPoint
public class MyViewGroup extends RelativeLayout {
    @Inject
    ViewUtil viewUtil;

    public MyViewGroup(Context context) {
        super(context);
        init();
    }

    public MyViewGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        Log.d("yu--", "viewGroup ViewScoped " + viewUtil);
    }
}
