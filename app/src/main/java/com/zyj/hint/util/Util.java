package com.zyj.hint.util;

import android.content.Context;

import com.zyj.hint.R;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;

public class Util {

    public String token = "1234444";
    public Context context;

    @Inject
    public Util(@ApplicationContext Context context) {
        this.context = context;
    }

    public String getName() {
        return context.getResources().getString(R.string.app_name);
    }
}
