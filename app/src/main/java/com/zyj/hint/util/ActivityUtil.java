package com.zyj.hint.util;

import javax.inject.Inject;
import dagger.hilt.android.scopes.ActivityScoped;

/**
 * @Description:
 * @author: zhaoyj
 * @date: 2022/12/24
 */
@ActivityScoped
public class ActivityUtil {
    String token = "1234444";

    @Inject
    ActivityUtil() {

    }
}
